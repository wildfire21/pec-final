using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Este script controla el comportamiento de la IA
public class AIHandler : MonoBehaviour
{
    public UnitController unitController;
    public UnitController enemy;
    public Phase myPhase;

    public float minDeadTime;
    public float maxDeadTime;

    //Tiempo de espera para que la IA ejecute la siguiente accion
    float getDeadTimeRate
    {
        get
        {
            float v = Random.Range(minDeadTime, maxDeadTime);
            return v;
        }
    }
    float deadTime;

    float attackTime = 1;
    public float attackRate = 1.5f;
    public float attackDistance = 2;
    public float stopDistance = 2;
    public float rotateDistance = 2;
    public float verticalThreshold = .1f;
    public float rotationThreshold = .5f;
    public float forceStopDistance = .3f;
    public bool forceStop;
    
    //Obtener si la IA esta interactuando con el jugador
    public bool isInteracting
    {
        get
        {
            return unitController.isInteracting;
        }
    }
    void Start()
    {
        unitController.isAI = true;
        unitController.onDeath = UnRegisterMe;

        //A�adir el Game Object a la fase
        if (myPhase != null)
        {
            myPhase.RegisterUnit(this);
        }
    }

    //Eliminar el Game Object de la fase
    void UnRegisterMe()
    {
        if (myPhase != null)
        {
            myPhase.UnRegisterUnit(this);
        }
    }

    void Update()
    {
        if (enemy == null) return;

        float delta = Time.deltaTime;
        
        //Posicion del Game Object que contenga este script (enemigo controlado por la IA)
        Vector3 myPosition = transform.position;

        //Posicion del objetivo al que debe dirigirse (posicion del jugador)
        Vector3 enemyPosition = enemy.position;

        if (isInteracting || unitController.isDead)
        {
            unitController.UseRootMotion(delta);
            return;
        }

        if (deadTime > 0 )
        {
            deadTime -= delta;
            return;
        }

        //Direction a la que se encuentra el objetivo
        Vector3 directionToTarget = enemyPosition - myPosition;
        directionToTarget.Normalize();
        directionToTarget.z = 0;

        //Posicion del objetivo
        Vector3 targetPosition = enemyPosition + (directionToTarget * -1) * attackDistance;

        //Control de rotacion
        bool isCloseToTargetPosition = IsCloseToTargetPosition(myPosition, targetPosition);
        bool isCloseToEnemy_NoVertical = IsCloseToEnemy_NoVertical(myPosition, enemyPosition);
        bool isCloseToEnemy_General = IsCloseToEnemy_General(myPosition, enemyPosition);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(myPosition, forceStopDistance);
        forceStop = false;
   
        //Pone en espera a todos los demas enemigos que no esten interactuando con el jugador
        foreach (var item in colliders)
        {
            AIHandler a = item.transform.GetComponentInParent<AIHandler>();
            if (a != null)
            {
                if (a != this)
                {
                    if (!a.forceStop)
                    {
                        forceStop = true;
                    }
                }
            }
        }

        //Controlar rotacion
        Vector3 targetDirection = Vector3.zero;
        if (!forceStop && !isCloseToTargetPosition && !isCloseToEnemy_NoVertical)
        {
            targetDirection = targetPosition - transform.position;
            targetDirection.Normalize();

            if (!isCloseToEnemy_General) unitController.HandleRotation(targetDirection.x < 0);
            else unitController.HandleRotation(directionToTarget.x < 0);
        }
        else
        {
            unitController.HandleRotation(directionToTarget.x < 0);
            
            if (attackTime > 0)
            {
                if (!forceStop) attackTime -= delta;
            }
            else
            {
                //Ejecuta una accion
                unitController.PlayAction(unitController.actionDataHolder.actions[0].actions[0]);
                attackTime = attackRate;
                deadTime = getDeadTimeRate;
            }
        }

        //Movimiento
        unitController.TickPlayer(delta, targetDirection);
    }

    //Controla si esta cerca del objetivo
    public bool IsCloseToTargetPosition(Vector3 p1, Vector3 p2)
    {
        float distance = Vector3.Distance(p1, p2);
        return distance < stopDistance;
    }

    //Controla si esta cerca del enemigo sin tener el cuenta la altura
    public bool IsCloseToEnemy_NoVertical(Vector3 p1, Vector3 p2)
    {
        float dif = p1.y - p2.y;
        if (Mathf.Abs(dif) < verticalThreshold)
        {
            return Vector3.Distance(p1, p2) < attackDistance;
        }
        else return false;
    }

    //Controla si esta cerca del enemigo
    public bool IsCloseToEnemy_General(Vector3 p1, Vector3 p2)
    {
        return Vector3.Distance(p1, p2) < rotateDistance;
    }
}
