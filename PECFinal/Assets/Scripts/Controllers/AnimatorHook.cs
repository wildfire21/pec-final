using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHook : MonoBehaviour
{
    Animator animator;
    public Vector3 deltaPosition;
    UnitController owner;

    //Controla si el unitController puede hacer un combo
    public bool canEnableCombo
    {
        get
        {
            return animator.GetBool("canEnableCombo");
        }
    }

    //Controla si el unitController esta interactuando
    public bool isInteracting
    {
        get
        {
            return animator.GetBool("isInteracting");
        }
    }
    void Start()
    {
        animator = GetComponent<Animator>();
        owner = GetComponentInParent<UnitController>();
    }

    //Reproducir animacion del unitController
    public void PlayAnimation(string animName, float crossfadeTime = 0)
    {
        if (crossfadeTime > 0.01f)
        {
            animator.CrossFadeInFixedTime(animName, crossfadeTime);
        }
        else
        {
            animator.Play(animName);
        }
        animator.SetBool("isInteracting", true);
    }

    //Controla si el unitController se esta moviendo
    public void Tick(bool isMoving)
    {
        float v = (isMoving) ? 1 : 0;
        animator.SetFloat("move", v);
    }

    private void OnAnimatorMove()
    {
        deltaPosition = animator.deltaPosition / Time.deltaTime;
    }

    //Controla si el unitController ha muerto
    public void SetIsDead()
    {
        animator.SetBool("isDead", true);
    }

    //Controla si el unitController puede hacer un combo
    public void SetIsCombo()
    {
        animator.SetBool("isCombo", true);
    }

    /*public void CloseAgent()
    {
        //owner.agent.enabled = false;
    }

    public void OpenAgent()
    {
        //owner.agent.enabled = true;
    }*/

    //Controla si el unitController esta interactuando
    public void SetIsInteracting(int status)
    {
        animator.SetBool("isInteracting", status == 1);
    }

    //Carga acciones al unitController
    public void LoadActionData(int actionIndex)
    {
        owner.LoadActionData(actionIndex);
    }
}
