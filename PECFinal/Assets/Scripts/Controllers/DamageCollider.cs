using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCollider : MonoBehaviour
{
    UnitController owner;
    void Start()
    {
        owner = GetComponentInParent<UnitController>();
    }

    void Update()
    {
        
    }

    ////Controla si la hit box del unitController entra en contacto con la hurt box de otro unitController
    private void OnTriggerEnter2D(Collider2D other)
    {
        UnitController unitController = other.GetComponentInParent<UnitController>();
     
        if (unitController != null)
        {
            if (unitController != owner)
            {
                if (unitController.team != owner.team || owner.getLastAction.canHitAllies)
                {
                    unitController.OnHit(owner.getLastAction, owner.isLookingLeft, owner);
                }
            }
        }
    }
}
