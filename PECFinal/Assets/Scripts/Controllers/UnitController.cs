using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitController : MonoBehaviour
{
    GameManager gameManager;
    public int health;
    public int team;
    AnimatorHook animatorHook;
    SoundManager soundManager;
    HealthUI healthUI;
    public Transform holder;
    
    public float horizontalSpeed = .8f;
    public float verticalSpeed = .65F;
    public bool isLookingLeft;
    public bool isAI;
    public bool hasBackHit;
    public bool isDead;
    public LayerMask walkLayer;

    public delegate void OnDeath();
    public OnDeath onDeath;

    //Controla la posicion del unitController
    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }

    //Controla si el unitController puede hacer un combo
    public bool canDoCombo
    {
        get
        {
            return animatorHook.canEnableCombo;
        }
    }

    public int actionsIndex;
    public ActionDataHolder actionDataHolder;

    //Controla las acciones del unitController
    ActionData[] currentActionData
    {
        get
        {
            return actionDataHolder.GetActions(actionsIndex);
        }
    }

    //Controla si el unitController esta interactuando
    public bool isInteracting
    {
        get
        {
            return animatorHook.isInteracting;
        }
    }
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        animatorHook = GetComponentInChildren<AnimatorHook>();
        soundManager = FindObjectOfType<SoundManager>();
        healthUI = FindObjectOfType<HealthUI>();

        if (!isAI) health = gameManager.health;
    }

    //Movimiento del unitController
    public void TickPlayer(float delta, Vector3 direction)
    {
        direction.x *= horizontalSpeed * delta;
        direction.y *= verticalSpeed * delta;
        bool isMoving = direction.sqrMagnitude > 0;
        animatorHook.Tick(isMoving);

        Vector3 targetPosition = transform.position + direction;
        MoveOnPosition(targetPosition);
    }

    //Movimiento del unitController
    public void UseRootMotion(float delta)
    {
        Vector3 targetPosition = transform.position + animatorHook.deltaPosition * delta;
        MoveOnPosition(targetPosition);
    }

    //Movimiento del unitController
    void MoveOnPosition(Vector3 targetPosition)
    {
        Collider2D[] colliders = Physics2D.OverlapPointAll(targetPosition, walkLayer);
        bool isValid = false;
        
        foreach (var item in colliders)
        {
            if (!isAI)
            {
                TBlockMovement block = item.GetComponent<TBlockMovement>();
                if (block != null)
                {
                    isValid = false;
                    break;
                }
            }

            TBlockMovementLevel blockLevel = item.GetComponent<TBlockMovementLevel>();
            if (blockLevel != null)
            {
                isValid = false;
                break;
            }

            TWalkable w = item.GetComponent<TWalkable>();
            if (w != null)
            {
                if (isAI)
                {
                    isValid = true;
                }
                else
                {
                    if (w.isPlayer)
                    {
                        isValid = true; 
                    }
                }
            }
        }

        if (isValid)
        {
            transform.position = targetPosition;
        }
    }

    //Rotacion del unitController
    public void HandleRotation(bool looksLeft)
    {
        Vector3 eulers = Vector3.zero;
        isLookingLeft = false;
        if (looksLeft)
        {
            eulers.y = 180;
            isLookingLeft = true;
        }
        holder.localEulerAngles = eulers;
    }

    //Obtiene la ultima accion del unitController
    ActionData storedAction;
    public ActionData getLastAction
    {
        get
        {
            return storedAction;
        }
    }

    //Obtiene la accion actual del unitController
    public void DetectAction(InputHandler.InputFrame f)
    {
        if (f.attack == false && f.jump == false) return;

        foreach (var a in currentActionData)
        {
            if (a.isDeterministic)
            {
                if (a.inputs.right == f.right
                    && a.inputs.left == f.left
                    && a.inputs.up == f.up
                    && a.inputs.down == f.down
                    && a.inputs.attack == f.attack
                    && a.inputs.jump == f.jump)
                {
                    PlayAction(a);
                    break;
                }
            }
            else
            {
                if (a.inputs.attack == f.attack
                    || a.inputs.jump == f.jump)
                {
                    PlayAction(a);
                    break;
                }
            }
        }
    }

    //Reproduce una accion del unitController
    public void PlayAction(ActionData actionData)
    {
        PlayAnimation(actionData.actionAnim);
        PlaySound(actionData.actionSound);
        storedAction = actionData;
    }

    //Reproduce una animacion del unitController
    public void PlayAnimation(string animName, float crossfadeTime = 0)
    {
        animatorHook.PlayAnimation(animName, crossfadeTime);
    }

    //Reproduce un sonido del unitController
    public void PlaySound(string soundName)
    {
        soundManager.PlaySound(soundName);
    }

    //Controla si el unitController ha muerto
    public void SetIsDead()
    {
        animatorHook.SetIsDead();
        isDead = true;
        
        onDeath?.Invoke();
    }

    //Controla los sucesos cuando una hit box colisiona con una hurt box
    public void OnHit(ActionData actionData, bool hitterLooksLeft, UnitController attacker)
    {
        if (isDead) return;

        //Controla si el golpe es por la espalda
        bool isFromBehind = false;
        if (isLookingLeft && hitterLooksLeft || !hitterLooksLeft && !isLookingLeft)
        {
            isFromBehind = true;
        }

        DamageType damageType = actionData.damageType;
        health -= actionData.damage;
        if (!isAI) gameManager.health = health;
        if (!isAI) healthUI.Damage(actionData.damage);
        
        //Si la vida del unitController es igual o menor a 0, el tipo de da�o siempre sera heavy
        if (health <= 0)
        {
            if (damageType != DamageType.bounce) damageType = DamageType.heavy;
            SetIsDead();
        }

        if (!hasBackHit)
        {
            if (isFromBehind)
            {
                HandleRotation(!hitterLooksLeft);
            }

            isFromBehind = false;
        }

        Vector3 eulers = Vector3.zero;

        //Control del tipo de da�o producido por la accion del unitController y reproduce la animacion, el sonido y la rotacion correspondiente
        switch (damageType)
        {
            
            case DamageType.light:
                if (isFromBehind && isLookingLeft)
                {
                    eulers.y = 0;
                    holder.localEulerAngles = eulers;
                }
                else if (isFromBehind && !isLookingLeft)
                {
                    eulers.y = 180;
                    holder.localEulerAngles = eulers;
                }
                PlayAnimation("Hurt1", 0.1f);
                if (!isAI) PlaySound("hurt1");
                else PlaySound("hurt2");
                break;
            case DamageType.mid:
                if (isFromBehind && isLookingLeft)
                {
                    eulers.y = 0;
                    holder.localEulerAngles = eulers;
                }
                else if (isFromBehind && !isLookingLeft)
                {
                    eulers.y = 180;
                    holder.localEulerAngles = eulers;
                }
                PlayAnimation("Hurt1");
                break;
            case DamageType.heavy:
                if (isFromBehind && isLookingLeft)
                {
                    eulers.y = 0;
                    holder.localEulerAngles = eulers;
                }
                else if (isFromBehind && !isLookingLeft)
                {
                    eulers.y = 180;
                    holder.localEulerAngles = eulers;
                }
                PlayAnimation("KnockDown");
                if (!isAI) PlaySound("hurt3");
                else PlaySound("hurt4");
                break;
            case DamageType.bounce:
                if (isFromBehind && isLookingLeft)
                {
                    eulers.y = 0;
                    holder.localEulerAngles = eulers;
                }
                else if (isFromBehind && !isLookingLeft)
                {
                    eulers.y = 180;
                    holder.localEulerAngles = eulers;
                }
                PlayAnimation("SchoolGirlBounce", 0.1f);
                PlaySound("hurt4");
                break;
            default:
                break;
        }

        //Reproduce una animacion antes de acabar la anterior
        if (actionData.onHitOverrideMyAnimation)
        {
            attacker.PlayAnimation(actionData.myOverrideAnimation, actionData.crossfadeTime);
        }
    }

    //Controla el combo del unitController
    public void isCombo()
    {
        animatorHook.SetIsCombo();
    }

    //Carga de acciones del unitController
    public void LoadActionData(int index)
    {
        actionsIndex = index;
    }

    //Resetea las acciones
    public void ResetActionData()
    {
        actionsIndex = 0;
    }
}
