using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Asigna el Script ActionData a un objeto tipo ActionDataHolder
[CreateAssetMenu]
public class ActionDataHolder : ScriptableObject
{
    public ActionsContainer[] actions;

    public ActionData[] GetActions(int index)
    {
        return actions[index].actions;
    }
}

[System.Serializable]
public class ActionsContainer
{
    public ActionData[] actions;
}
