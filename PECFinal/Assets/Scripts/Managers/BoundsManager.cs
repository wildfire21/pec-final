using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script para controlar los limites para el movimiento de la camara
public class BoundsManager : MonoBehaviour
{
    private BoxCollider2D bounds;
    private CameraManager cameraManager;
    void Start()
    {
        bounds = GetComponent<BoxCollider2D>();
        cameraManager = FindObjectOfType<CameraManager>();
        cameraManager.SetBounds(bounds);
    }
}
