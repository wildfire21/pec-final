using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controla el movimiento de la camara siguiendo al jugador
public class CameraManager : MonoBehaviour
{
    public Transform target;

    public BoxCollider2D boundBox;
    private Vector3 minBounds;
    private Vector3 maxBounds;

    private new Camera camera;
    private float halfHeight;
    private float halfWidth;

    private void Start()
    {
        minBounds = boundBox.bounds.min;
        maxBounds = boundBox.bounds.max;

        camera = GetComponentInChildren<Camera>();
        halfHeight = camera.orthographicSize;
        halfWidth = halfHeight * Screen.width / Screen.height;
    }

    private void Update()
    {
        Vector3 tp = Vector3.Lerp(transform.position, target.position, Time.deltaTime * 6);
        tp.z = transform.position.z;
        transform.position = tp;

        if (boundBox == null)
        {
            boundBox = FindObjectOfType<BoundsManager>().GetComponent<BoxCollider2D>();
            minBounds = boundBox.bounds.min;
            maxBounds = boundBox.bounds.max;
        }

        float clampedX = Mathf.Clamp(transform.position.x, minBounds.x + halfWidth, maxBounds.x - halfWidth);
        float clampedY = Mathf.Clamp(transform.position.y, minBounds.y + halfHeight, maxBounds.y - halfHeight);
        transform.position = new Vector3(clampedX, clampedY, tp.z);
    }

    public void SetBounds(BoxCollider2D newBounds)
    {
        boundBox = newBounds;

        minBounds = boundBox.bounds.min;
        maxBounds = boundBox.bounds.max;
    }
}
