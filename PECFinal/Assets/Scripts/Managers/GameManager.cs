using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controla la vida del jugador
public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public int health = 100;
    GameOver defeat;
    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        defeat = FindObjectOfType<GameOver>();
        defeat.gameObject.SetActive(false);
    }
}
