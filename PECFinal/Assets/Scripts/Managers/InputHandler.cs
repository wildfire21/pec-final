using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script para la asignacion de los controles
public class InputHandler : MonoBehaviour
{
    public UnitController unitController;

    public InputFrame inputFrame;
    void Start()
    {
        
    }

    void Update()
    {
        inputFrame.left = false;
        inputFrame.right = false;
        inputFrame.up = false;
        inputFrame.down = false;
        inputFrame.attack = false;
        inputFrame.jump = false;

        //Movimiento
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        //Ataque
        inputFrame.attack = Input.GetButtonDown("Fire1");

        //Salto
        inputFrame.jump = Input.GetButtonDown("Fire2");

        //Controla cual de los botones de movimiento se esta pulsando
        if (h > .2f) inputFrame.right = true;
        if (h < -.2f) inputFrame.left = true;
        if (v > .2f) inputFrame.up = true;
        if (v < -.2f) inputFrame.down = true;

        //Movimiento
        Vector3 targetDirection = Vector3.zero;
        targetDirection.x = h;
        targetDirection.y = v;

        if (unitController.isInteracting)
        {
            if (unitController.canDoCombo)
            {
                if (inputFrame.attack)
                {
                    unitController.isCombo();
                }
            }

            unitController.UseRootMotion(Time.deltaTime);
        }

        else
        {
            if (targetDirection.x != 0)
            {
                unitController.HandleRotation(targetDirection.x < 0);
            }

            unitController.TickPlayer(Time.deltaTime, targetDirection);
            unitController.DetectAction(inputFrame);
        }
    }

    [System.Serializable]
    public class InputFrame
    {
        public bool left;
        public bool right;
        public bool up;
        public bool down;
        public bool attack;
        public bool jump;
    }
}
