using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Controla la navegacion entre escenas al collisionar con un Trigger collider
public class NextSceneTigger : MonoBehaviour
{
    public string nextScene;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var inp = collision.transform.GetComponentInParent<UnitController>();
        if (inp != null)
        {
            if (!inp.isAI)
            {
                SceneManager.LoadScene(nextScene);
            }
        }
    }
}
