using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Script que define la fase de un nivel
public class Phase : MonoBehaviour
{
    public List<AIHandler> currentUnits = new List<AIHandler>();

    public UnityEvent onPhaseStart;
    public UnityEvent onPhaseEnded;

    //Registra un enemigo activo
    public void RegisterUnit(AIHandler a)
    {
        currentUnits.Add(a);
    }

    //Elemina un enemigo derrotado
    public void UnRegisterUnit(AIHandler a)
    {
        currentUnits.Remove(a);

        if (currentUnits.Count == 0)
        {
            onPhaseEnded.Invoke();
        }
    }
}
