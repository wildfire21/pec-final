using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Control de las fases del nivel
public class PhaseManager : MonoBehaviour
{
    Phase currentPhase;

    public Phase debugPhase;
    public bool debugStart;

    private void Update()
    {
        if (debugStart)
        {
            debugStart = false;
            AssingPhase(debugPhase);
        }
    }

    public void AssingPhase(Phase p)
    {
        currentPhase = p;
        currentPhase.onPhaseStart.Invoke();
    }
}