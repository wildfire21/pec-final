using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Control de efectos de sonido
public class SoundManager : MonoBehaviour
{
    private AudioClip attack1, upAttack, downAttack, sideAttack, jump, jumpAttack, hurt1, hurt2, hurt3, hurt4;
    AudioSource audioSource;
    void Start()
    {
        attack1 = Resources.Load<AudioClip>("Sounds/attack1");
        upAttack = Resources.Load<AudioClip>("Sounds/upAttack");
        downAttack = Resources.Load<AudioClip>("Sounds/downAttack");
        sideAttack = Resources.Load<AudioClip>("Sounds/sideAttack");
        jump = Resources.Load<AudioClip>("Sounds/jump");
        jumpAttack = Resources.Load<AudioClip>("Sounds/jumpAttack");
        hurt1 = Resources.Load<AudioClip>("Sounds/hurt1");
        hurt2 = Resources.Load<AudioClip>("Sounds/hurt2");
        hurt3 = Resources.Load<AudioClip>("Sounds/hurt3");
        hurt4 = Resources.Load<AudioClip>("Sounds/hurt4");
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        
    }

    public void PlaySound(string clipName) 
    {
        switch (clipName)
        {
            case "attack1":
                audioSource.PlayOneShot(attack1);
                break;
            case "upAttack":
                audioSource.PlayOneShot(upAttack);
                break;
            case "downAttack":
                audioSource.PlayOneShot(downAttack);
                break;
            case "sideAttack":
                audioSource.PlayOneShot(sideAttack);
                break;
            case "jump":
                audioSource.PlayOneShot(jump);
                break;
            case "jumpAttack":
                audioSource.PlayOneShot(jumpAttack);
                break;
            case "hurt1":
                audioSource.PlayOneShot(hurt1);
                break;
            case "hurt2":
                audioSource.PlayOneShot(hurt2);
                break;
            case "hurt3":
                audioSource.PlayOneShot(hurt3);
                break;
            case "hurt4":
                audioSource.PlayOneShot(hurt4);
                break;
            default:
                break;
        }
    }
}
