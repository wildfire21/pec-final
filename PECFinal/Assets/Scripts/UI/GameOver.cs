using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controla los sucesos cuando el jugador pierde
public class GameOver : MonoBehaviour
{
    private AudioClip gameOver;
    AudioSource audioSource;
    SoundManager soundManager;
    public GameObject restartMenu;
    void Start()
    {
        gameOver = Resources.Load<AudioClip>("Sounds/game_over");
        soundManager = FindObjectOfType<SoundManager>();
        audioSource = soundManager.GetComponent<AudioSource>();
        audioSource.Stop();
        audioSource.PlayOneShot(gameOver);
        Time.timeScale = 0f;
        restartMenu.gameObject.SetActive(true);
    }

    void Update()
    {
        
    }
}
