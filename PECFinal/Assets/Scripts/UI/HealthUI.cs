using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Controla la animacion de la barra de vida de la UI dependiendo de la vida del unitController
public class HealthUI : MonoBehaviour
{
    public UnitController unitController;
    public Image[] healthPoints;
    float health, maxHealth;
    void Start()
    {
        maxHealth = unitController.health;
        health = maxHealth;
    }

    void Update()
    {
        if (health > maxHealth) health = maxHealth;
        HealthBarFiller();
    }

    void HealthBarFiller()
    {
        for (int i = 0; i < healthPoints.Length; i++)
        {
            healthPoints[i].enabled = !DisplayHealthPoint(health, i);
        }
    }

    public void Damage(float damagePoints)
    {
        if (health > 0)
        {
            health -= damagePoints;
        }
    }

    bool DisplayHealthPoint(float _health, int pointNumber)
    {
        return ((pointNumber * 4) >= _health);
    }
}
