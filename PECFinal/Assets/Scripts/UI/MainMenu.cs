using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Control del menu principal
public class MainMenu : MonoBehaviour
{
    public Canvas canvas;
    GameManager gameManager;
    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }
    private void Update()
    {
        if (Input.anyKey)
        {
            canvas.gameObject.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
    public void PlayGame()
    {
        SceneManager.LoadScene("Level1");
    }

    public void TitleScreen()
    {
        SceneManager.LoadScene("TitleScreen");
        gameManager.health = 100;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
