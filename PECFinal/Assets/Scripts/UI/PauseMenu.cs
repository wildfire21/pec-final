using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Control del menu de pausa
public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            PauseGame();
        }
    }
    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void TitleScreen()
    {
        SceneManager.LoadScene("TitleScreen");
        gameManager.health = 100;
        Time.timeScale = 1f;
    }
}
