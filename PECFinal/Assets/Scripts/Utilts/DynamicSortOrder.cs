using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controla el sortingOrder de los sprites dependiendo de su posicion
public class DynamicSortOrder : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Transform origin;
    void Start()
    {
        
    }

    void Update()
    {
        Vector3 position = transform.position;
        if (origin != null)
        {
            position = origin.position;
        }

        int sortOrder = Mathf.RoundToInt(position.y * 100 + 10);
        spriteRenderer.sortingOrder = -sortOrder;
    }
}
