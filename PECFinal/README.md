# PEC Final
Para la practica final para la asignatura de programaci�n de videojuegos 2D decid� realizar un videojuego de genero Beat'em Up, inspirado en los juego de River City Girls 1 y 2.
Este consta de una primera versi�n de juego, con 2 escenarios. En esta practica los escenarios actuan como niveles pero la idea seria que en un juego completo formaran parte de una serie de escenarios 
que conformarian algo similar a un mundo abierto.
La mec�nica principal del juego consiste en avanzar por los escenarios mientras el jugador derrota a los enemigos que le salen al paso y llegar al final del ultimo escenario sin ser derrotado antes.
El trabajo realizado en la practica no busca una gran cantidad de contenido, sino montar los cimientos de lo que en un futuro podria convertirse en un juego completo. Se ha desarrollado el core del juego y las
mecanicas principales de tal forma que todo a quedado dispuesto para que, a partir de este core, pueda seguir a�adiendose contenido facilmente. A partir del trabajo realizado es facil a�adir nuevos movimientos y
combos para el jugador, mas tipos de enemigos con mas tipos de acciones, jefes finales, mas esenarios, argumento e historia, etc.

COMO SE JUEGA
Movimiento del jugador: Flechas de direccion.
Ataque: Ctrl.
Salto: Alt.
Movimientos especiales: Flechas de direccion/Alt + Ctrl. 

ESTRUCTURA DEL JUEGO
Los sprites, fondos, personajes, sonidos, se han obtenido de las paginas: http://www.spriters-resource.com/ y https://www.sounds-resource.com/  
Consta de 4 escenas:
Escena de t�tulo, con botones para pasar a la escena de nivel 1, ver los controles y cerrar el juego.
Escenas para el nivel 1 y escena para el nivel 2.
Escena de cr�ditos.

IMPLEMENTACION
-Game Manager: En esta practica, el Game Manager solo controla la cantidad de vida del jugador, de momento.
-Camera Manager: Contiene el componente Camera Manager (Script) que controla el movimiento de la camara siguiendo al jugador. Contiene los objetos Main Camera y Sound Manager. El componente Camera Manager (Script)
bloquea el movimiento de la camara cuando el jugador entra en una zona determinada y lo desbloquea cuando se cumplen los requisitos.
	-Main Camera: Contiene los objetos Lock y Lock Chain (inactivo al inicio).
		-Lock: Contiene la animacion de un candado para cuando se bloquea el movimiento de la camara. Contiene un objeto que emite el sonido correspondiente de la animacion.
		-LockChain: Contiene dos Objetos Left y Right que contienen componentes Box Collider 2D para impedir que el jugador se salga del angulo de vision de la camara Contiene los objetos para la animacion de unas 
		cadenas para cuando se bloquea el movimiento de la camara. Contiene un objeto que emite el sonido correspondiente de la animacion.
		-Sound Manager: Contiene el componente Sound Manager (Script) y un componente AudioSource. El componente AudioSource reproduce la musica de fondo. El componente Sound Manager (Script) controla algunos efectos
		de sonido por codigo.
-Canvas: Contiene el HUD y los diferentes menus (inactivos). Al pulsar la tecla Esc se abre el menu de pausa. Si el jugador muere, timeScale pasa a ser 0 y se activa el menu de Restart.
-Level Renderer: Contiene los sprites y el Sort Order Pivot del escenario.
-Bounds: Contiene un componente Box Collider 2D que delimita la zona por la que puede moverse la camara.
-Walkable Area: Contiene un componente Box Collider 2D y T Walkable (Script) que delimitan la zona por la que pueden moverse el jugador y los enemigos de la IA.
-Phase Manager: Contiene los objetos Phase que conforman las fases dentro de un nivel y controla su logica con el componente Phase Manger (Script).
	-Objetos Phase: Controlan la logica del comportamiento del juego en cada fase dentro del nivel con el componente Phase Manger (Script).
	-Objetos Phase Trigger: Controlan la logica de los objetos Phase con el componente On Trigger Event (Script) cuando el jugador entra en contacto con el componente Box Collider2D.
Tanto el personaje principal como los enemigos de la IA estan contolados por el script UnitController para la logica basica.
El comportamiento de los enemigos de la IA esta controlado por el script AIHandler.
Tanto el personaje principal como los enemigos de la IA estan compuestos de la siguiente manera:
	-Objeto Unit Controller: Objeto padre.
		-Objecto Holder: Contiene el objeto que maneja el animator.
			-Objeto Kyoko/SchoolGirl: controla las animaciones y el sorting order. Contiene los siguiente objetos:
				-SpriteRender.
				-Hurt Box y Hit Box: ambas el conjunto controlan la colision de los golpes del personaje principal y los enemigos de la IA. Hit Box solo se activa cuando se realiza un ataque.
				-Sort order pivot: Objeto para controlar el sorting order en el objeto Kyoko/SchoolGirl.
				-Sounds: Contiene objetos con componentes AudioClip inactivos.
					-Objetos con componente AudioClip: Se activan y desactivan controlados por animaciones.
				-Defeat object: Inactivo, se activa al termino de la animacion de muerte del jugador y ejecuta el componente Game Over (Script), que define la logica de cuando muere el juegador.

Link al video demostracion de YouTube: https://youtu.be/KEnzE4S_d0U
